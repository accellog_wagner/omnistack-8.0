import axios from 'axios';

const api = axios.create({
    baseURL: 'http://192.168.43.21:3333'
});

export default api;

// utilizar para vincular as portas de conexao app + api
// adb reverse tcp:3333 tcp:3333