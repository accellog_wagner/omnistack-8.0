const { Schema, model } = require('mongoose');

// schema da tabela do banco de dados
const ProductSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    }, 
    type: {
        type: Schema.Types.ObjectId,
        ref: 'ProductType',
        required: true,
    },    
}, {
    timestamps: true,
});

module.exports = model('Product', ProductSchema);