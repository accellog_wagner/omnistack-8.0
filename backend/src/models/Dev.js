const { Schema, model } = require('mongoose');

// schema da tabela do banco de dados
const DevSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    user: {
        type: String,
        required: true,
    },
    bio: String,
    avatar: {
        type: String,
        required: true,
    },
    // relacionamento com os ids 'foreignkeys'
    likes: [{
        type: Schema.Types.ObjectId,
        ref: 'Dev',
    }],
    dislikes:  [{
        type: Schema.Types.ObjectId,
        ref: 'Dev',
    }],
}, {
    // cria colunas created, updated e popula seus valores automaticamente
    timestamps: true,
});

module.exports = model('Dev', DevSchema);