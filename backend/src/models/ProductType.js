const { Schema, model } = require('mongoose');

const ProductTypeSchema = new Schema({
    type: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    }   
}, {
    timestamps: true,
});

module.exports = model('ProductType', ProductTypeSchema);