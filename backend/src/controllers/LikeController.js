const Dev = require('../models/Dev');

module.exports = {
    // rota POST
    async store(req, res) {
        // pegando valores do header
        const { user } = req.headers;
        // pegando valores da url
        const { devId } = req.params;

        // realizando consultas no banco de dados
        const loggedDev = await Dev.findById(user);
        const targetDev = await Dev.findById(devId);

        // validando user não cadastrado
        if (!targetDev) {
            return res.status(400).json({ error: 'Dev not exists' });
        }   
        
        // validando se possui o id do user que deu like
        if (targetDev.likes.includes(loggedDev._id)) {
            const loggedSocket = req.connectedUsers[user];
            const targetSocket = req.connectedUsers[devId];

            if (loggedSocket) {
                req.io.to(loggedSocket).emit('match', targetDev);
            }

            if (targetSocket) {
                req.io.to(targetSocket).emit('match', loggedDev);
            }
        }

        // adicionando valor ao array de likes
        loggedDev.likes.push(targetDev._id);
        // salvando o PUT no user
        await loggedDev.save();

        return res.json(loggedDev);
    }
};