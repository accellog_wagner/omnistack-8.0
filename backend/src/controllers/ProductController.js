const axios = require('axios');
const Product = require('../models/Product');
const ProductType = require('../models/ProductType');

module.exports = {
    // rota GET ALL
    async index(req, res) {
        
        const products = await Product.find();        
        
        if (products.length < 0) {
            return res.status(404).json({ error: 'Not Found.' }); 
        }      

        return res.json(products);
    },

    // rota POST
    async store(req, res) {                
        const { name, description, price, type } = req.body;           

        const typeExists = await ProductType.findById({ _id: type });

        if (!typeExists) {
            return res.status(404).json({ error: 'Product Type does not exist.' }); 
        }

        const productExists = await Product.findOne({ name: name });

        if (productExists) {
            return res.json(productExists);
        }    
          
        const prod = await Product.create({
            name: name,
            description: description,
            price: price,
            type: type,
        });
                
        return res.json(prod);
    },

    async update(req, res) {
        const { id, name, description, price, type } = req.body;

        const productExists = await Product.findOne({ name: name });

        if (!productExists) {
            return res.status(404).json({ error: 'Product does not exist.' }); 
        } 

        const typeExists = await ProductType.findById({ _id: type });

        if (!typeExists) {
            return res.status(404).json({ error: 'Product Type does not exist.' }); 
        }        

        const prod = await Product.updateOne({
            _id: id,
            name: name,
            description: description,
            price: price,
            type: type,
        });
                
        return res.json(prod);
    },
};