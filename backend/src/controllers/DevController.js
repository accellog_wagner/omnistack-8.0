const axios = require('axios');
const Dev = require('../models/Dev');

module.exports = {
    // rota GET
    async index(req, res) {
        // pegando valores do header
        const { user } = req.headers;

        // busca das informações do user
        const loggedDev = await Dev.findById(user);

        // query no banco de dados
        const users = await Dev.find({
            $and: [
                // ne => not equal
                { _id: { $ne: user } },
                // nin => not in 
                { _id: { $nin: loggedDev.likes } },
                { _id: { $nin: loggedDev.dislikes } },
            ],
        });
        
        return res.json(users);
    },
    // rota POST
    async store(req, res) {                
        const { username } = req.body;

        // busca do user
        const userExists = await Dev.findOne({ user: username });
        // caso já tenha cadastrado
        if (userExists) {
            return res.json(userExists);
        }

        // consulta na api do github, buscando as informações
        const response = await axios.get(`https://api.github.com/users/${username}`);

        if (!response.data.name) {
            name = response.data.login;
        } else {
            name = response.data.name;
        }
        // mapeando as variáveis com os valores do response
        const { bio, avatar_url: avatar } = response.data;

        // chamando metodo create do DevController
        const dev = await Dev.create({
            name: name,
            user: username,
            bio: bio,
            avatar: avatar,
        });
                
        return res.json(dev);
    }
};