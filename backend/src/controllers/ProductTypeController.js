const axios = require('axios');
const ProductType = require('../models/ProductType');

module.exports = {
    // rota GET ALL
    async index(req, res) {
        
        const productTypes = await ProductType.find();        
        
        return res.json(productTypes);
    },

    // rota POST
    async store(req, res) {                
        const { type, description } = req.body;           

        const productTypeExists = await ProductType.findOne({ type: type });

        if (productTypeExists) {
            return res.json(productTypeExists);
        }                       
        const prodType = await ProductType.create({
            type: type,
            description: description,           
        });
       
        return res.json(prodType);
    }
};