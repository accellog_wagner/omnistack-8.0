const Dev = require('../models/Dev');

module.exports = {
    // rota POST
    async store(req, res) {
        // pegando valores do header
        const { user } = req.headers;

        // pegando valores da url
        const { devId } = req.params;

        // realizando consultas no banco de dados
        const loggedDev = await Dev.findById(user);
        const targetDev = await Dev.findById(devId);

        // validando user não cadastrado
        if (!targetDev) {
            return res.status(400).json({ error: 'Dev not exists' });
        }        

        // adicionando valor ao array de dislikes
        loggedDev.dislikes.push(targetDev._id);
        // salvando o PUT no user
        await loggedDev.save();

        return res.json(loggedDev);
    }
};