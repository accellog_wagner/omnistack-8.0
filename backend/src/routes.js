const express = require('express');
const DevController = require('./controllers/DevController');
const LikeController = require('./controllers/LikeController');
const DislikeController = require('./controllers/DislikeController');
const ProductController = require('./controllers/ProductController');
const ProductTypeController = require('./controllers/ProductTypeController');

const routes = express.Router();

// rotas controller de products
routes.get('/productType', ProductTypeController.index);
routes.post('/productType', ProductTypeController.store);

// rotas controller de products
routes.get('/products', ProductController.index);
routes.post('/products', ProductController.store);
routes.put('/products', ProductController.update);

// rotas controller de devs
routes.get('/devs', DevController.index);
routes.post('/devs', DevController.store);

// rotas controller de likes 
routes.post('/devs/:devId/likes', LikeController.store);

// rotas controller de dislikes
routes.post('/devs/:devId/dislikes', DislikeController.store);

module.exports = routes;